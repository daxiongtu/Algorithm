#include <iostream>  
#include <algorithm> 
#define MAX 5  
using namespace std; 
int D[MAX][MAX] = {
	{7},
 	{3, 8}, 
 	{8, 1, 0},
 	{2, 7, 4, 4},
 	{4, 5, 2, 6, 5}
};  



/*
				7

			3		8

		8		1		0

	2		7		4		4

 4		5		2		6		5


在上面的数字三角形中寻找一条从顶部到底边的路径，使得路径上所经过的数字之和最大。
路径上的每一步都只能往左下或 右下走。只需要求出这个最大和即可，不必给出具体路径。 
三角形的行数大于1小于等于100，数字为 0 - 99

*/

/* 
接下来，我们来分析一下解题思路：

首先，肯定得用二维数组来存放数字三角形

然后我们用D( r, j) 来表示第r行第 j 个数字(r,j从1开始算)

我们用MaxSum(r, j)表示从D(r,j)到底边的各条路径中，最佳路径的数字之和。

因此，此题的最终问题就变成了求 MaxSum(1,1)

当我们看到这个题目的时候，首先想到的就是可以用简单的递归来解题：

D(r, j)出发，下一步只能走D(r+1,j)或者D(r+1, j+1)。
故对于N行的三角形，我们可以写出如下的递归式：   

	if ( r == N)                
		MaxSum(r,j) = D(r,j)  
	else      
		MaxSum( r, j) = Max{ MaxSum(r＋1,j), MaxSum(r+1,j+1) } + D(r,j)
*/
int n = MAX - 1;  
int MaxSum(int i, int j){    
	if(i == n)  
		return D[i][j];    
	int x = MaxSum(i + 1, j);    
	int y = MaxSum(i + 1, j + 1);    
	cout << "max = " << max(x, y) << " current = "<< D[i][j] << endl;
	return max(x, y) + D[i][j];  
}

void caculateMaxSum() {
	for(int i = 0; i <= n; i++)  {
		for(int j = 0; j <= n; j++){
			cout << D[i][j] << " ";    
		}       	
		cout << "\n---" << endl;
	} 
		
	cout << MaxSum(0, 0) << endl; 
}



// 优化版本1
int maxSum[MAX][MAX] = {0};

int MaxSum2(int i, int j) {
	if (maxSum[i][j] != -1) {
		return maxSum[i][j];
	} else if (i == n) {
		maxSum[i][j] = D[i][j];
	} else {
		int x = MaxSum2(i + 1, j);
		int y = MaxSum2(i + 1, j + 1);
		maxSum[i][j] = max(x, y) + D[i][j];
	}

	return  maxSum[i][j];
}
void caculateMaxSum2() {
	for(int i = 0; i <= n; i++)  {
		for(int j = 0; j <= n; j++){
			maxSum[i][j] = -1;    
		}       	
	}
	cout << MaxSum2(0, 0) << endl; 
}

int main(int argc, const char **argv){    
	 caculateMaxSum2();
}