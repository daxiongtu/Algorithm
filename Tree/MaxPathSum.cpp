#include <iostream>
#include <math.h>
#include <algorithm>

using namespace std;
/**
 * Definition for a binary tree node.
 */
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
static int sum = 0;

// 根据数组创建二叉树
TreeNode *creatBTree(int data[], int index, int n) {
    TreeNode * pNode = NULL;
    //数组中-1 表示该节点为空
    if(index < n && data[index] != -1) {
        pNode = (TreeNode *)malloc(sizeof(TreeNode));
        if(pNode == NULL) return NULL;
        pNode->val = data[index];

        //将二叉树按照层序遍历, 依次标序号, 从0开始
        pNode->left = creatBTree(data, 2 * index + 1, n); 
        pNode->right = creatBTree(data, 2 * index + 2, n);
    }
    return pNode;
}


/* 遍历思想：
 * 前序遍历：根结点 ---> 左子树 ---> 右子树
 * 中序遍历：左子树 ---> 根结点 ---> 右子树
 * 后序遍历：左子树 ---> 右子树 ---> 根结点
 * 层次遍历：只需按层次遍历即可
 *
 */
// 前序遍历
void preOrderTraverse(TreeNode *root) {
    if (root != NULL) {
        cout << root->val << "\t";
        preOrderTraverse(root->left);
        preOrderTraverse(root->right);
    }
}

// 中序遍历
void inOrderTraverse(TreeNode *root) {
    if (root != NULL) {
        inOrderTraverse(root->left);
        cout << root->val << "\t";
        inOrderTraverse(root->right);
    }
}

// 后序遍历
void postOrderTraverse(TreeNode *root) {
    if (root != NULL) {
        postOrderTraverse(root->left);
        postOrderTraverse(root->right);
        cout << root->val << "\t";
    }
}

class Solution {
public:
    int maxPathSum(TreeNode* root) {
        if (root->left == NULL && root->right == NULL) return root->val;
        value(root);
        return sum;
    }
    int value(TreeNode* node) {
        if (node == NULL) return 0;
        int leftValue = max(value(node->left), 0);
        int rightValue = max(value(node->right), 0);
        
        // 路径累计求和
        sum = max(sum, node->val + leftValue + rightValue);
        return node->val + max(leftValue, rightValue);
    }    
};

int main(int argc, char const *argv[]) {
    int n = 8;
    int array[8] = {1, 2, 3, 4, 5, 6, 7, 8};
    TreeNode *root = creatBTree(array, 0, n);
    preOrderTraverse(root);
    cout << endl;
    inOrderTraverse(root);
    cout << endl;
    postOrderTraverse(root);
    cout << endl;

    return 0;
}