
#include<iostream>
using namespace std;
/*
 * a为数组指针
 * n 为数组大小
 * p 为左移位数
 * 该算法的时间复杂度为O(n)，空间复杂度为O(p).
 */ 
void shift1(int *a, int n, int p);

// 该算法的时间复杂度为O(n)，空间复杂度为O(1).
void shift2(int *a, int n, int p);

// 数组数据右移p位
void rightShift(int *a, int n, int p);

int main(int argc, const char **argv) {
	const int n = 10, p = 3;
	int array[n] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	rightShift(array, n, p);
	
	return 0;
}
void printArray(int *a, int n) {
	for (int i = 0; i < n; i++) {
		cout << a[i] << " ";
	}
	cout << endl;
}
// 建立一个大小为p的一维数组，存放数组A的前p个元素，
// 然后将A中的p~n-1个元素向前移动p个单位，然后将将p中的数组放回A中，得到B
// 该算法的时间复杂度为O(n)，空间复杂度为O(p).
void shift1(int *a, int n, int p) {
	int b[p];
	int i;
	// 1：前p位数据存入b数组
	for(i = 0; i < p; i++)  b[i] = a[i];

	// 2：p+1 到n的数据整体向前移动p位
	for(; i < n; i++)  a[i-p] = a[i];

    // 3：b的数据以此添加到数组a的n-p到n的范围
	for(i = 0; i < p;i++)  a[n-p+i] = b[i];

	// 4：打印数组
	printArray(a, n);
}
void reverse(int *a, int left, int right) {
	cout << "reverse " << left << " --> " << right << ":\t";
	int mid = (left + right)/2;
	int temp;
	for(int i = 0; i <= (mid - left); i++) {
		temp = a[left+i];
		a[left+i] = a[right-i];
		a[right-i] = temp;	
	}
}

// 我们将A的前p个元素看做a,剩下的n-p个元素看做b，则A=ab，B=ba，
// 我们先对A逆置得到b^-1 a^-1，然后再对b^-1和a^-1逆置得到ba。
// 该算法的时间复杂度为O(n)，空间复杂度为O(1).
void shift2(int *a, int n, int p) {
	//先对A进行逆置得到b^-1 a^-1
	reverse(a, 0, n - 1);	
	printArray(a, n);

	//对b^-1进行逆置
	reverse(a, 0, n - 1 - p);
	printArray(a, n);

	//对a^-1进行逆置
	reverse(a, n - p, n - 1);
	printArray(a, n);
}

// 数组数据右移p位
// 思路：我们将数组A中的后p个元素记为b，前n-p个元素记为a,
// 则A=ab,B=ba，我们先对A逆置得到b^-1 a^-1，然后再对b^-1和a^-1逆置得到ba。
void rightShift(int *a, int n, int p) {
	//先对A进行逆置得到b^-1 a^-1
	reverse(a, 0, n - 1);
	printArray(a, n);

	//对b^-1进行逆置
	reverse(a, 0, p - 1);
	printArray(a, n);

	//对a^-1进行逆置
	reverse(a, p, n - 1);
	printArray(a, n);
}

