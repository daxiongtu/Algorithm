## 简单选择排序

#### 基本思想

* 循环n次。每次循环找出第i项以后的数中最小的数，与第i项进行交换
* 每次循环开始时前i项时排好序的
* 在要排序的一组数中，选出最小（或者最大）的一个数与第1个位置的数交换；然后在剩下的数当中再找最小（或者最大）的与第2个位置的数交换，依次类推，直到第n-1个元素（倒数第二个数）和第n个元素（最后一个数）比较为止。

```
//挑选出最小值的下标
int selectMinKey(int array[],int n, int i)
{
    int k = i;
    for(int j=i+1;j<n;j++)
    {
        if(array[k] > array[j]) k = j;
    }
    return k;
}
//第i次循环找出i以后的数中的最小值，与当前第i项进行交换，每次循环的前i项是已经排好序的
void SelectSort(int array[], int n)
{
    int key,temp;
    for(int i=0;i<n;i++)
    {
        key = selectMinKey(array, n, i);
        if(key != i)
        {
            temp = array[i];
            array[i] = array[key];
            array[key] = temp;//将第i个位置的元素与最小值互换
        }
        print(array, n, i);
    }
}
```