//
//  AddTwoNumbers.cpp
//  LeetcodeSet
//
//  Created by xujin on 2020/8/29.
//  Copyright © 2020 xujin. All rights reserved.
//

#include "AddTwoNumbers.hpp"
#include <stdlib.h>
#include <string.h>

/*
 输入：(2 -> 4 -> 3) + (5 -> 6 -> 4)
 输出：7 -> 0 -> 8
 原因：342 + 465 = 807
*/

// 生成链表
ListNode *generateLink(int *array, size_t size) {
    ListNode *pHead = NULL;
    ListNode *current = NULL;
    for (int i = 0; i < size; i++) {
        ListNode *node = (ListNode *)malloc(sizeof(ListNode));
        node->next = NULL;
        node->val = array[i];
        if (current == NULL) {
            current = node;
            pHead = current;
        } else {
            current->next = node;
            current = current->next;
        }
    }
    return pHead;
}

void print(ListNode *p) {
    while (p != NULL) {
        printf("%d\t", p->val);
        p = p->next;
    }
    printf("\n");
}

ListNode* AddTwoNumbers::addTwoNumbers(ListNode* l1, ListNode* l2) {
    ListNode *head = new ListNode(0);
    ListNode *p = l1;
    ListNode *q = l2;
    ListNode *current = head;
    
    // 进位
    int carray = 0;
    while (p != NULL || q != NULL) {
        int x = (p != NULL) ? p->val : 0;
        int y = (q != NULL) ? q->val : 0;
        int sum = carray + x + y;
        carray = sum/10;
        current->val = sum % 10;
        if (p != NULL) p = p->next;
        if (q != NULL) q = q->next;
        
        if (p != NULL || q != NULL) {
            current->next = new ListNode(0);
            current = current->next;
        }
    }
    if (carray > 0) {
        current->next = new ListNode(0);
        current->next->val = carray;
    }
    
    return head;
}

void AddTwoNumbers::run() {
    int first[] = {2, 4, 3};
    int second[] = {5, 6, 4};
    size_t first_size = sizeof(first)/sizeof(first[0]);
    size_t second_size = sizeof(second)/sizeof(second[0]);
    ListNode *p1 = generateLink(first, first_size);
    ListNode *p2 = generateLink(second, second_size);
    print(p1);
    print(p2);
    ListNode *result = addTwoNumbers(p1, p2);
    print(result);
}
