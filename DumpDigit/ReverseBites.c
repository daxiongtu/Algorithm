#include <stdio.h>
int main(int argc, const char **argv) {
	int number = 5;
	int bites = sizeof(int) * 8;

	for (int i = bites - 1; i > 0; i-- ) {
		number = ((number >> i) ^ 1 ) << i;
	}
	printf("%d\n", number);


	return 0;
}