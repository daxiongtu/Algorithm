//
//  LengthOfLongestSubstring.cpp
//  LeetcodeSet
//
//  Created by xujin on 2020/8/30.
//  Copyright © 2020 xujin. All rights reserved.
//

#include "LengthOfLongestSubstring.hpp"
#include <unordered_set>

int LengthOfLongestSubstring::max(int a, int b) {
    if (a > b) {
        return a;
    }
    return b;
}

int LengthOfLongestSubstring::substringLength(std::string s) {
    // 哈希集合，记录每个字符是否出现过
    std::unordered_set<char> occ;
    size_t n = s.size();

    // 右指针，初始化为-1，相当于我们在字符串的左边界的左侧，还没开始移动
    int rk = -1, ans = 0;

    // 枚举左指针的位置，初始值隐形地表示为-1
    for(int i = 0; i < n; ++i) {
        if (i != 0) {
            // 左指针向右移动一格，移除一个字符
            occ.erase(s[i-1]);
        }
        while(rk + 1 < n && !occ.count(s[rk+1])) {
            occ.insert(s[rk+1]);
            ++rk;
        }

        // 第i到rk个字符是一个极长的无重复字符子串
        ans = max(ans, rk - i + 1);
    }
    return ans;
}
void LengthOfLongestSubstring::run() {
    std::string input = "abcabcbb";
    int length = substringLength(input);
    std::cout << length << std::endl;
}
