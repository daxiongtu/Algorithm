//
//  TwoSum.hpp
//  LeetcodeSet
//
//  Created by xujin on 2020/8/29.
//  Copyright © 2020 xujin. All rights reserved.
//

#ifndef TwoSum_hpp
#define TwoSum_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <unordered_map>


/*
 1. 两数之和  难度 简单
 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。

 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。

 示例:

 给定 nums = [2, 7, 11, 15], target = 9
 因为 nums[0] + nums[1] = 2 + 7 = 9
 所以返回 [0, 1]
 */

/*
 解题思路：
 Map<Key, Value> result =  key 为nums[i] ,value: i(下标)
 1：result  查找 key = target - nums[i] 的数据，查找到，直接返回[key, value]
 2：未查找到，resulp.put(nums[i], i)
 3：goto 1
 
 */

class TwoSumSolution {
private:
    std::vector<int> twoSum(std::vector<int> &nums, int target);
public:
    void run();
    TwoSumSolution() {}
    ~TwoSumSolution() {}
};

#endif /* TwoSum_hpp */
