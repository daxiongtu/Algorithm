#include <stdio.h>
#include <stdlib.h>

// 链表结点结构体
typedef struct Node {
	int value;
	struct Node *previous;
	struct Node *next;
} Node;

// 创建链表
struct Node *createList(int *array, int n) {
	// 头结点定义
	Node *head = NULL;

	// 记录当前尾结点
	Node *currentNode = NULL;

	for (int i = 0; i < n; i++)  {
		// 创建新的节点
		Node *node = (Node *)malloc(sizeof(struct Node));
		node->value = array[i];
		node->next = NULL;

		// 头结点为空，新结点即为头结点
		if (head == NULL) {
			head = node;
		} else {
			// 当前结点的next为新结点
			currentNode->next = node;
		}

		// 设置当前结点为新结点：相当于当前结点后移
		currentNode = node;
	}
	return head;
}

// 链表长度
int listLength(Node *head) {
	if (head == NULL) return 0;
	int index = 0;
	Node *node = head;
	while(node) {
		index++;
		node = node->next;
	}
	return index;
}

// 链表是否为空
int isEmpty(Node *head) {
	if (head == NULL) return 1;
	return 0;
}

// 翻转链表
Node *reverseList(Node *head) {
	// 遍历链表的结点
	Node *node = head;

	// 新链表的头结点
	Node *newHead = NULL;

	while(node != NULL) {
		// 保存当前结点的下一个结点
		Node *nextNode = node->next;

		// 当前结点的next指向新链表的头结点
		node->next = newHead;

		// 新链表的头结点向后移动
		newHead = node;

		// 当结点向后移动
		node = nextNode;
	}
	return newHead;
}

/* 
 * 数据插入position
 * head：链表头部
 * value： 插入的数据
 * position: 插入的位置
 * 	1：position < 0 插入头部
 * 	2：position > list_len ,插入尾部
 * 	3：插入链表中间
 */ 
void insertNode(Node *head, int value, int position) {
	if (head == NULL) return;

	// 数据插入头部
	if (position <= 0) {
		// 1：分配插入结点内存
		Node *insertNode = (Node *)malloc(sizeof(struct Node));
		if (insertNode == NULL) return;

		// 2：head的value赋值给新建结点
		insertNode->value = head->value;

		// 3：新结点的next指针指向header的下一个结点
		insertNode->next = head->next;

		// 4：head的next指针指向新建的结点
		head->next = insertNode;

		// 5：value赋值给head结点
		head->value = value;

		return;
	}

	Node *node = head;
	Node *tail = node;
	int index = 0;

	while(node != NULL) {
		if (++index == position) {
			// 分配插入结点内存
			struct Node *insertNode = (struct Node *)malloc(sizeof(struct Node));
			if (insertNode == NULL) return;
			insertNode->value = value;

			// 插入结点next指向node的next结点
			insertNode->next = node->next;

			// node的next结点指向新创建的插入结点
			node->next = insertNode;
			return;
		}
		printf("index = %d, position = %d\n", index, position);
		tail = node;
		node = node->next;
	}

	// 超出链表长度，数据就加入尾部
	Node *insertNode = (Node *)malloc(sizeof(Node));
	if (insertNode == NULL) return;
	insertNode->value = value;
	insertNode->next = NULL;
	tail->next = insertNode;
}

/* 清空链表
 * 1：函数一定要传入二级指针，否则无法改变指针的地址值，即无法删除指针；
 * 2：注意不要漏掉最后一个结点；
 * 3：这个函数也进行了错误判断和处理；
 */
void freeList(Node **head) {
	if (*head == NULL) return;

	Node *node = *head;
	// 循环释放链表中的结点所占内存
	while ((*head)->next != NULL) {
		node = (*head)->next;
		free((*head));
		(*head) = node;
	}

	// 清除最后一个结点
	if ((*head) != NULL) {
		free((*head));
		(*head) = NULL;
	}
}

// 打印链表（遍历链表）
void printList(Node *head) {
	Node *node = head;
	while(node != NULL) {
		printf("%d ", node->value);
		node = node->next;
	}
	printf("\n");
}

// 链表排序
void swap(int *a, int *b) {
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

Node *partion(Node *pBegin, Node *pEnd){
    if(pBegin == pEnd || pBegin->next == pEnd)  return pBegin;

    //选择pBegin作为基准元素
    int key = pBegin->value;    
    Node *p = pBegin, *q = pBegin;

    //从pBegin开始向后进行一次遍历
    while(q != pEnd){   
        if(q->value < key){
            p = p->next;
            swap(&p->value, &q->value);
        }
        q = q->next;
    }
    swap(&p->value, &pBegin->value);
    return p;
}
void quickSort(Node *pBegin, Node *pEnd){
    if(pBegin == pEnd || pBegin->next == pEnd) return;
    Node *mid = partion(pBegin, pEnd);
    quickSort(pBegin, mid);
    quickSort(mid->next, pEnd);
}

Node *sortList(Node *head) {
    if(head == NULL || head->next == NULL) return head;
    quickSort(head, NULL);
    return head;
}
// 链表合并（链表有序）
Node *mergeList(Node *head1, Node *head2) {
	if (head1 == NULL) return head2;
	if (head2 == NULL) return head1;

	Node *newHead = NULL;
	Node *p1 = head1;
	Node *p2 = head2;
	if (head1->value < head2->value) {
		newHead = head1;
		p1 = head1->next;
	} else {
		newHead = head2;
		p2 = head2->next;
	}

	Node *tmpNode = newHead;
	while(p1 != NULL && p2 != NULL) {
		if (p1->value < p2->value) {
			tmpNode->next = p1;
			tmpNode = p1;
			p1 = p1->next;
			
		} else {
			tmpNode->next = p2;
			tmpNode = p2;
			p2 = p2->next;
		}
	}
	// tmpNode的next指针指向两个链表任一不为空的部分
	if (p1 != NULL) {
		tmpNode->next = p1;
	} else {
		tmpNode->next = p2;
	}
	return newHead;
}

void mergeSotedList() {
	int array[] = {9, 1, 8, 7, 19, 20, 88, 100, 81, 27, 65, 72};
	int n = sizeof(array)/sizeof(array[0]);
	// 创建链表
	printf("创建链表2:\t");
	Node* head = createList(array, n);
	sortList(head);
	printList(head);

	int array2[] = {77, 72, 70, 61, 82, 49, 13, 42, 991, 1024, 801, 501};
	int n2 = sizeof(array2)/sizeof(array2[0]);
	// 创建链表
	printf("创建链表2:\t");
	Node* head2 = createList(array2, n2);
	sortList(head2);
	printList(head2);

	printf("合并有序链表\t");
	Node *newHead = mergeList(head, head2);
	printList(newHead);
}

// 链表删除结点
void deleteNode(Node **head, int value) {
	if (head == NULL) return;
	Node *node = *head;
	// 处理只有一个结点的情况
	if (node->next == NULL) {
		if (node->value == value) {
			free((*head));
			(*head) = NULL;
		}
		return;
	}
	// 删除头结点，并使头结点指针指向下一个结点的位置
	if (node->value == value) {
		(*head) = node->next;
		free(node);
		node = NULL;
		return;
	}
	// 保存上一个结点
	Node *previousNode = node;
	while(node) {
		// 删除结点
		if (node->value == value) {
			previousNode->next = node->next;
			free(node);
			node = NULL;
			break;
		}
		previousNode = node;
		// 当前结点移动到后一个结点的位置
		node = node->next;
	}
}
// 环链表
// 检测环链表入口
// 获取单链表第K个结点
Node *findKPositionNode(Node *head, int kPosition) {
	if (head == NULL) return head;
	int index = 0;
	Node *node = head;
	while(node) {
		if (index++ == kPosition) {
			return node;
		} 
		node = node->next;
	}
	return NULL;
}
// 获取单链表倒数第K个结点
Node *findTailKPositionNode(Node *head, int kPosition) {
	if (head == NULL) return head;
	int index = -1;
	Node *fast = head;
	Node *slow = head;
	while(fast) {
		if (index++ == kPosition) {
			break;
		}
		fast = fast->next->next;
		slow = slow->next;
	}
	if (index < kPosition) {
		return NULL;
	} 
	while(fast) {
		fast = fast->next;
		slow = slow->next; 
	}
	return slow;
}

// 查找第一个和Value匹配的结点
Node *findNode(Node *head, int value) {
	if (head == NULL) return NULL;
	Node *node = head;
	while (node) {
		if (node->value == value) {
			return node;
		}
		node = node->next;
	}
	return NULL;
}

// 结点交换:找到两个结点，交换value即可
void swapTwoNode(Node *head, int value1, int value2) {
	if (head == NULL) return;
	Node *node1 = findNode(head, value1);
	Node *node2 = findNode(head, value2);
	// 任一结点不存在
	if (node1 == NULL || node2 == NULL) return;
	node1->value = value2;
	node2->value = value1;
}

// 双向链表

int main(int argc, char const *argv[]) {
	int array[] = {9, 1, 8, 7, 19, 20, 88, 100, 81, 27, 65, 72};
	int n = sizeof(array)/sizeof(array[0]);
	// 创建链表
	printf("创建链表:\t");
	Node* head = createList(array, n);
	printList(head);

	// 插入结点
	int position = 0;
	insertNode(head, 123, position);
	printf("向第%d个位置插入结点\t", position);
	printList(head);

	// 链表排序（快排）
	printf("链表排序\t");
	sortList(head);
	printList(head);

	// 翻转链表
	printf("翻转链表:\t");
	head = reverseList(head);
	printList(head);

	// 删除结点
	printf("删除结点\t");
	deleteNode(&head, 9);
	printList(head);

	// 查找第K个结点
	printf("查找第K个结点，");
	int kPosition = 5;
	Node *kNode = findKPositionNode(head, kPosition);
	if (kNode != NULL) {
		printf("第%d个结点的值为：%d\n", kPosition, kNode->value);
	} else {
		printf("未找到第%d个结点\n", kPosition);
	}

	// 查找倒数第K个结点
	kPosition = 1;
	Node *tailKNode = findTailKPositionNode(head, kPosition);
	if (tailKNode != NULL) {
		printf("倒数第%d个结点的值为：%d\n", kPosition, tailKNode->value);
	} else {
		printf("未找到倒数第%d个结点\n", kPosition);
	}

	printf("链表长度\t");
	int length = listLength(head);
	printf("链表长度 = %d\n", length);

	// 交换结点
	int value1 = 123, value2 = 1;
	printf("交换两个结点(%d, %d)\n", value1, value2);
	printf("交换前\t");
	printList(head);
	swapTwoNode(head, value1, value2);
	printf("交换后\t");
	printList(head);

	// 清空链表
	printf("清空链表\n");
	freeList(&head);
	printList(head);

	// 合并有序链表
	mergeSotedList();

	return 0;
}