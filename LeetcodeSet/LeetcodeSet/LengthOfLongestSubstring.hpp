//
//  LengthOfLongestSubstring.hpp
//  LeetcodeSet
//
//  Created by xujin on 2020/8/30.
//  Copyright © 2020 xujin. All rights reserved.
//

#ifndef LengthOfLongestSubstring_hpp
#define LengthOfLongestSubstring_hpp

/*
 题目：
 给定一个字符串，请你找出其中不含有重复字符的最长子串的长度

 示例1：
 输入："abcabcbb"
 输出：3
 解释：因为无重复字符的最长子串是"abc",所以其长度为3

 示例2：
 输入："bbbbb"
 输出：1
 解释：因为无重复字符的最长子串是"b",所以其长度为1

 示例3：
 输入："pwwkew"
 输出：3
 解释：因为无重复字符的最长子串是"wke",所以其长度为3，
     请注意，你的答案必须是子串的长度，"pwke"是一个子序列，不是子串


 方法一：滑动窗口

 思路和算法
 我们先用一个例子想想如何在较优的时间复杂度内通过本体


 我们不妨以示例一中的字符换串abcabcbb为例，找出从每一个字符开始的，不包含重复字符的最长子串，那么其中最长的那个字符串即为答案。
 对于示例一种的字符串，我们列举出这些结果，其中括号中表示选中的字符以及最长的字符串
 * 以 (a)bcabcbb 开始的最长字符串为 (abc)abcbb
 * 以 a(b)cabcbb 开始的最长字符串为 a(bca)bcbb；
 * 以 ab(c)abcbb 开始的最长字符串为 ab(cab)cbb；
 * 以 abc(a)bcbb 开始的最长字符串为 abc(abc)bb；
 * 以 abca(b)cbb 开始的最长字符串为 abca(bc)bb；
 * 以 abcab(c)bb 开始的最长字符串为 abcab(cb)b；
 * 以 abcabc(b)b 开始的最长字符串为 abcabc(b)b；
 * 以 abcabcb(b) 开始的最长字符串为 abcabcb(b)

 发现了什么？如果我们依次递增地枚举子串的起始位置，那么子串的结束位置也是递增的！这里的原因在于，假设我们选择字符串中的第
 k 个字符作为起始位置，并且得到了不包含重复字符的最长子串的结束位置为rk。那么当我们选择第 k+1 个字符作为起始位置时，首先从
 k+1 到 rk的字符显然是不重复的，并且由于少了原本的第 k 个字符，我们可以尝试继续增大 rk，直到右侧出现了重复字符为止。

 这样以来，我们就可以使用「滑动窗口」来解决这个问题了：

 * 我们使用两个指针表示字符串中的某个子串（的左右边界）。其中做指针代表着上文中【枚举子串的起始位置】，而有指针即为上文中的rk
 * 在每一步的操作中，我们会将做指针向右移动一格，表示我们枚举下一个字符作为起始位置，然后我们可以不断地向右移动右指针，但需要
   保证这两个指针对应的子串中没有重复的字符。在移动结束后，这个子串就对应着以左指针开始的，不包含重复字符的最长子串。
   我们记录下这个子串的长度；

 判断重复字符
 在上面的流程中，我们还需要使用一种数据结构来判断是否有重复字符，常用的数据结构有哈希集合（即C++中的std::unordered_set,Java
 中的HashSet,Python中的set,JavaScript中的Set）。在做指针向右移动的时候，我们从哈希集合中移除一个字符，
 在右指针向右移动的时候，我们网哈希集合中添加一个字符

 至此完美解决
 */


#include <stdio.h>
#include <string>
#include <iostream>

class LengthOfLongestSubstring {
private:
    int substringLength(std::string s);
    int max(int a, int b);
public:
    void run();
};

#endif /* LengthOfLongestSubstring_hpp */
