#include "MemoryManager.h" 
#include <iostream>

using namespace std;

/*
 * 字符串复制
 * 1：src和dst无重叠，直接从src开始往后复制
 * 2：dst指向src区间，直接从src结尾往前复制
*/
void *mymemcpy(void *dst, const void *src, size_t num) {
	if (dst == NULL || src == NULL) return NULL;

	char *pdst = (char *)dst;
	const char *psrc = (const char *)src;

	// dst与src有重叠且dst指向src中的元素，因此，需要对src从后向前复制
	if (pdst > psrc && pdst < (psrc + num)) {
		// 重新分配内存空间，避免原始dst指针访问未知的区域
		pdst = (char *)malloc(sizeof(char) * num);
		for (size_t i = num - 1; i != - 1; i--) {
			pdst[i] = psrc[i];
		}
	} else {
		// src和dst没有重叠，从前往后复制
		for(size_t i = 0; i < num; i++) {
			pdst[i] = psrc[i];
		}
	}
	return dst;
}

// 内存操作
void memTest() {
	char src[] = "abcdefghijklmn";
	char *dst = src + 1;
	size_t size = sizeof(src)/sizeof(src[0]);
	dst = (char *)mymemcpy(dst, src, size);
	printf("%s\n", dst);
}