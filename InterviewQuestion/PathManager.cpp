#include "PathManager.h"

#include <iostream>

using namespace std;

// 根据两个绝对路径算出相对路径
char *getRelativePath(char *path1, char *path2, char *relativePath) {
	if (path1 == NULL || path2 == NULL) {
		cout << "参数不合法" << endl;
		return NULL;
	}

	// 用来指向两个路径中不同目录的起始路径
	char *diff1 = path1;
	char *diff2 = path2;

	while(*path1 != 0 &&  *path2 != 0) {
		// 如果目录相同，则向后遍历
		if(*path1 == *path2) {
			if (*path1 == '/') {
				diff1 = path1;
				diff2 = path2;
			}
			path1++;
			path2++;
		} else {
			// 不同目录
			// 把path1 中的非公共部分的目录转化为../
			diff1++; // 跳过目录分隔符
			while(*diff1 != 0) {
				// 碰到下一级目录
				if (*diff1 == '/') {
					strcat(relativePath, "../");
				}
				diff1++;
			}

			// 把path2的非公共部分的路径添加到后面
			diff2++; // 跳过目录分隔符
			strcat(relativePath, diff2);
			break;
		}
	}
	return relativePath;
}

void relativePathTest() {
	char a[] = "/qihoo/app/a/b/c/news.c";
	char b[] = "/qihoo/app/1/2/test.c";
	char relativePath[256] = {0};
	cout << getRelativePath(a, b, relativePath) << endl;
}