#include "BinarySearchTree.h"
#include "Test.h"
#include <iostream>

using namespace std;

void testInOrder() {
	int num[] = {1, 2, 3, 4, 5, 6, 7, 8};
	Tree tree(num, 8);

	// 递归：中序遍历
	cout << "InOrder: ";
	tree.inOrderTree();

	// 非递归：中序遍历
	cout << "\nInOrder: ";
	tree.inOrderTreeUnRecursion();
	cout << "\n";
}

void testPreOrder() {
	int num[] = {5, 3, 7, 2, 4, 6, 8, 1};
	Tree tree(num, 8);

	// 递归：先序遍历
	cout << "PreOrder: ";
	tree.preOrderTree();

	// 非递归：先序遍历
	cout << "\nPreOrder: ";
	tree.preOrderTreeUnRecursion();
	cout << "\n";
}

void testPostOrder() {
	int num[] = {5, 3, 7, 2, 4, 6, 8, 1};
	Tree tree(num, 8);

	// 递归：后序遍历
	cout << "PostOrder: ";
	tree.postOrderTree();

	// 非递归：先序遍历
	cout << "\nPostOrder: ";
	tree.postOrderTreeUnRecursion();
	cout << "\n";
}

void testLevelOrder() {
	int num[] = {5, 3, 7, 2, 4, 6, 8, 1};
	Tree tree(num, 8);

	// 按层次遍历
	cout << "LevelOrder: ";
	tree.levelOrderTree();
	cout << "\n";
}

void testIsBinarySearchTree() {
	int num[] = {5, 3, 7, 2, 4, 6, 8, 1};
	Tree tree(num, 8);

	// 非递归中序遍历
	cout << "InOrder: ";
	tree.inOrderTreeUnRecursion();
	cout << "\n IsSortedTree " << tree.isSortedTree(tree) << endl;

	// 把值为4的节点的值修改为1
	Node *node = tree.searchNode(4);
	node->data = 1;
	cout << "InOrder: ";
	tree.inOrderTreeUnRecursion();
	cout << "\n IsSortedTree " << tree.isSortedTree(tree) << endl;
}

void invertTree() {
	int num[] = {5, 3, 7, 2, 4, 6, 8, 1};
	Tree tree(num, 8);

	// 非递归中序遍历
	cout << "InOrder: ";
	tree.inOrderTreeUnRecursion();

	// 翻转二叉树
	tree.invertTree();
	cout << "\nInOrder: ";
	tree.inOrderTreeUnRecursion();
	cout << endl;
}

void testRun() {
	invertTree();
}
