#include "TreeManager.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
using namespace std;

#define TRUE  1
#define FALSE 0

// IP 地址最多有11个不同的字符（0~9 和.）
#define CHAR_COUNT 11

// IP 地址的最大长度
#define MAX_IP 50

int getIndexFromChar(char c) { return (c == '.' ? 10 : (c - '0'));}
char getCharFromIndex(int i) { return (i == 10) ? '.' : ('0' + i);}

// Trie 树的结点
struct trieNode {

	// 是否是叶结点
	bool isLeaf;
	char *url;
	struct trieNode* child[CHAR_COUNT];
}trieNode;

typedef struct trieNode* trieNodeP;

// 创建一个新的Trie树结点
trieNodeP newTrieNode() {
	trieNodeP newNode = (trieNodeP)malloc(sizeof(trieNode));
	newNode->isLeaf = FALSE;
	newNode->url = NULL;

	for (int i = 0; i < CHAR_COUNT; i++) {
		newNode->child[i] = NULL;
	}
	return newNode;
}

// 把一个ip地址和对应的url添加到trie树中，最后一个结点是URL
void insert(trieNodeP root, char *ip, char *url) {
	// IP 地址的长度
	int len = strlen(ip);

	trieNodeP pCrawl = root;
	for (int level = 0; level < len; level++) {
		// 根据当前遍历到的ip中的字符，找出子节点的索引
		int index = getIndexFromChar(ip[level]);

		// 如果子节点不存在，就创建一个
		if (!pCrawl->child[index]) {
			pCrawl->child[index] = newTrieNode();
		}

		// 移动到子节点
		pCrawl = pCrawl->child[index];
	}

	// 在叶子节点中存储IP对应的url
	pCrawl->isLeaf = TRUE;
	pCrawl->url = (char *)malloc(strlen(url) + 1);
	strcpy(pCrawl->url, url);
}

char *searchDNSCache(trieNodeP root, char *ip) {
	trieNodeP pCrawl = root;
	int len = strlen(ip);
	int level ;

	// 遍历ip地址中的所有字符
	for (level = 0; level < len; level++) {
		int index = getIndexFromChar(ip[level]);
		if (!pCrawl->child[index]) {
			return NULL;
		}

		pCrawl = pCrawl->child[index];
	}

	// 返回找到url
	if (pCrawl != NULL && pCrawl->isLeaf) {
		return pCrawl->url;
	}
	return NULL;
}

void testTrie() {
	char ipAddress[][MAX_IP] = {
		"10.57.11.127", "121.57.61.129", "66.125.100.103", "192.168.0.103"
	};
	char url[][50] = {
		"www.samsung.com", "www.samsung.net", "www.google.in", "www.tencent.com"
	};

	int n = sizeof(ipAddress)/sizeof(ipAddress[0]);
	trieNodeP root = newTrieNode();

	// 把IP地址和对应的URL插入到trie树种
	for (int i = 0; i < n; i++) {
		insert(root, ipAddress[i], url[i]);
	}

	char ip[] = "121.57.61.129";
	char *result_url = searchDNSCache(root, ip);
	if (result_url != NULL)	{
		printf("%s --> %s\n", ip, result_url);
	} else {
		printf("没有找到对应url\n");
	}
}
