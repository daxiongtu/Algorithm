
#ifndef SORT_H

// 打印数组
void print(int a[], int n, int i);

// 插入排序
void InsertSort(int a[], int n);

// 冒泡排序
void BubbleSort(int a[], int n);

// 冒泡排序: 从后面到i个元素两两比较，把小的不断上顶 
void BubbleSort2(int array[], int n);

// 选择排序
void SelectSort(int array[], int n);

// 快速排序
void QuickSort(int array[], int left, int right);
void QuickSort2(int array[], int first, int end);

void sort();

#endif