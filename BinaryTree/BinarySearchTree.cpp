
#include "BinarySearchTree.h"

#include <stack>
#include <queue>
using namespace std;


// 插入num数组的前len个数
Tree::Tree(int num[], int len) {

	// 建立root节点
	root = new Node(num[0]);

	// 把数组中的其他数据插入二叉排序树中
	for (int i = 1; i < len; i++) {
		insertNode1(num[i]);
	}
}

// 插入数据为data的节点，非递归方法
void Tree::insertNode1(int data) {
	Node *p, *par;

	// 创建节点
	Node *newNode = new Node(data);

	p = par = root;

	// 查找插入在哪个节点下面
	while(p != NULL) {

		// 保存节点
		par = p;

		if (data > p->data) {
			// 如果data 大于当前节点的data，下一步到右节点，否则，进行到左节点
			p = p->right;
		} else if (data < p->data) {
			p = p->left;
		} else {
			// 不能插入重复节点
			delete newNode;
			newNode = NULL;
			return;
		}
	}

	newNode->parent = par;
	// 把新结点插入在目标结点的正确位置
	if (par->data > newNode->data) {
		par->left = newNode;
	} else {
		par->right = newNode;
	}
}
// 插入数据为data的节点，递归方法
void Tree::insertNode(int data) {
	if (root != NULL) {
		// 调用递归插入方法
		insertNode(root, data);
	}
}

// 查找节点
Node* Tree::searchNode(int data) {
	if (root != NULL) {
		// 调用递归搜索方法
		return searchNode(root, data);
	}
	return NULL;
}
// 删除节点
void Tree::deleteNode(int data) {
	// 查询data节点
	Node *current = searchNode(data);
	if (current != NULL) {
		// 调用递归删除方法
		deleteNode(current);
	}
}


// 递归插入方法
void Tree::insertNode(Node *current, int data) {
	// 如果data小于当前节点数据，则在当前节点左子树插入
	if (data < current->data) {
		if (current->left == NULL) {

			// 左节点不存在，则插入到左节点
			current->left = new Node(data);
			current->left->parent = current;
		} else {

			// 对左节点进行递归调用插入数据
			insertNode(current->left, data);
		}
	} else if (data > current->data) { // 如果data大于当前节点数据，则在右子树进行插入数据

		// 右节点不存在，则插入到右节点
		if (current->right == NULL) {

			current->right = new Node(data);
			current->right->parent = current;
		} else {

			// 对右节点进行递归调用插入数据
			insertNode(current->right, data);
		}
	} 

	// data 等于当前节点的数据时，不插入
	return;
}

// 递归查找方法
Node* Tree::searchNode(Node *current, int data) {
	// 如果data小当前节点数据，则在左子树查找，否则，在有子树查找
	if (data < current->data) {

		// 如果左子树不存在，返回NULL
		if (current->left == NULL) {
			return NULL;
		} else {
			// 递归查找左子树
			return searchNode(current->left, data);
		}
	} else if(data > current->data) { 

		// 如果右子树不存在，返回NULL
		if (current->right == NULL) {
			return NULL;
		} else {
			// 递归查找右子树
			return searchNode(current->right, data);
		}
	}

	// 相等，直接返回
	return current;
}

// 递归删除方法
void Tree::deleteNode(Node *current) {
 	// 删除左子树
 	if (current->left != NULL) {
 		deleteNode(current->left);

 	} else if(current->right != NULL) {
 		// 删除右子树
 		deleteNode(current->right);
 	}

 	if (current->parent == NULL) {
 		// 如果current 是根节点，把root置空
 		delete current;
 		root = NULL;
 		return;
 	}

 	// 将current父节点相应指针置空
 	if (current->parent->data > current->data) {
 		current->parent->left = NULL;
 	} else {
 		current->parent->right = NULL;
 	}

 	// 删除结点
 	delete current;
 	current = NULL;
}

// 中序遍历
/*
 * 1：遍历左子树
 * 2：访问根节点
 * 3：遍历右子树
*/ 
void Tree::inOrderTree() {
	if (root == NULL) { 
		return;
	}
	inOrderTree(root);
}
void Tree::inOrderTree(Node *current) {
	if (current != NULL) {
		// 1：遍历左子树
		inOrderTree(current->left);

		// 2：访问根节点
		cout << current->data << " ";

		// 3：遍历右子树
		inOrderTree(current->right);
	}
}

void Tree::inOrderTreeUnRecursion() {
	stack<Node *> s;
	Node *p = root;
	while(p != NULL || !s.empty()) {
		// 遍历左子树
		while (p != NULL) {
			// 把遍历的节点全部压栈
			s.push(p);
			p = p->left;
		}

		if (!s.empty()) {

			
			p = s.top(); 					// 得到栈顶内容
			s.pop(); 						// 出栈
			cout << p->data << " ";	// 打印
			p = p->right; 					// 指向右子节点，下次循环时就会中序遍历右子树
		}
	}
}

/*
 * 先序遍历
 * 1：访问根节点
 * 2：遍历左子树
 * 3：遍历右子树
*/
void Tree::preOrderTree() {
	if (root == NULL) {
		return;
	}
	preOrderTree(root);
}

void Tree::preOrderTree(Node *current) {
	if (current != NULL) {
		cout << current->data << " ";
		preOrderTree(current->left);
		preOrderTree(current->right);
	}
}

/* 非递归先序遍历
 * 1：打印根节点
 * 2：根节点right入栈，遍历左子树
 * 3：遍历完左子树返回时，栈顶原始为right，出栈，遍历以该指针文根的子树
 */
void Tree::preOrderTreeUnRecursion() {
	stack<Node *>s;
	Node *p = root;
	while(p != NULL || !s.empty()) {
		while(p != NULL) {
			cout << p->data << " "; // 打印
			s.push(p);	// 遍历的节点入栈
			p = p->left;
		}

		if (!s.empty()) {
			p = s.top();    // 得到栈顶内容
			s.pop();		// 出栈
			p = p->right;	// 指向右节点，下次循环时就会先序遍历左子树
		}
	}
}


/*
 * 后序遍历
 * 1：访问根节点
 * 2：遍历左子树
 * 3：遍历右子树
*/
// 递归方式
void Tree::postOrderTree() {
	if (root == NULL) {
		return;
	}
	postOrderTree(root);
}		

// 递归方式
void Tree::postOrderTree(Node *current) {
	if (current != NULL) {
		postOrderTree(current->left);	// 遍历左子树
		postOrderTree(current->right);	// 遍历右子树
		cout << current->data << " ";	// 打印节点数据
	}
}

// 非递归方式
void Tree::postOrderTreeUnRecursion() {
	stack<Node *>s;
	Node *p = root;

	while(p != NULL || !s.empty()) {
		while(p != NULL) {
			s.push(p);		// 压栈
			p = p->left;	// 遍历左子树
		}

		if (!s.empty()) {
			p = s.top();

			// tag 为1时
			if (p->tag == 1) { 
				cout << p->data << " "; // 打印
				s.pop();				// 出栈
				p = NULL;				// 第二次访问标志其右节点已经遍历
			} else {
				p->tag = 1;				// 修改tag 为1：说明已经访问过
				p = p->right;			// 指向右节点，下次遍历遍历起左子树
			}
		}
	}
}


// 按层次遍历二叉树
void Tree::levelOrderTree() {

	// 二叉树为空，直接退出
	if (root == NULL) return;

	// 定义队列q，他的元素为Node *类型指针
	queue<Node *> q;

	Node *ptr = NULL;

	// 根节点入队列
	q.push(root);

	while (!q.empty()) {
		ptr = q.front(); 	// 得到队头节点
		
		q.pop();			// 出队

		cout << ptr->data << " ";	// 打印

		// 当前节点存在左节点，则左节点入队列
		if (ptr->left != NULL) q.push(ptr->left);

		// 当前节点存在右节点，则右节点入队列
		if (ptr->right != NULL) q.push(ptr->right);
	}
}

// 是否为二叉排序树
bool Tree::isSortedTree(Tree tree) {
	int lastValue = 0;
	stack<Node *> s;
	Node *p = tree.root;

	while(p != NULL || !s.empty()) {
		while(p != NULL) { 	// 遍历左子树
			s.push(p);		// 把遍历的节点全部压栈
			p = p->left;
		}

		if (!s.empty()) {
			p = s.top();	// 得到栈顶的内容
			s.pop();		// 出栈

			// 如果是第一次弹出或lastValue小于当前节点的值
			// 则，给lastValue赋值
			if (lastValue == 0 || lastValue < p->data) {
				lastValue = p->data;
			} else if(lastValue >= p->data) {

				// 如果lastValue大于当前节点值，返回false
				return false;
			}

			// 指向右节点，下一次循环时就会中序遍历右子树
			p = p->right;
		}
	}
	return true;
}

// 翻转二叉树
/*
 * 翻转二叉树的步骤
 * 1：翻转根节点的左子树（递归调用当前函数）
 * 2：翻转根节点的右子树（递归调用当前函数）
 * 3：交换根节点的左子节点与右子节点
*/
Node *invertTreeIMP(Node *root) {
	// 为空，则终止
	if (root == NULL) return NULL;

	Node *node = root;

	// 翻转左子树
	invertTreeIMP(node->left);

	// 翻转右子树
	invertTreeIMP(node->right);

	// 交换左子节点与右子节点
	Node *tmpNode = node->left;
	node->left = node->right;
	node->right = tmpNode;
	return root;
}

void Tree::invertTree() {
	invertTreeIMP(root);
}

