
#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#include <iostream>

// 节点定义
class Node {
public:
	int data;		// 数据
	Node *parent; 	// 父节点
	Node *left;		// 左子节点
	Node *right;	// 右子节点
	int tag;

public:
	Node() : data(-1), parent(NULL), left(NULL), right(NULL) {};
	Node(int num): data(num), parent(NULL), left(NULL), right(NULL) {};
};

// 二叉排序树类定义
class Tree {
public:
	Tree(int num[], int len);	// 插入num数组的前len个数
	void insertNode1(int data);	// 插入节点，递归方法
	void insertNode(int data);	// 插入节点，非递归方法
	Node *searchNode(int data);	// 查找节点
	void deleteNode(int data);	// 删除节点

	// 遍历
	// 中序遍历
	void inOrderTree();					// 递归方式
	void inOrderTreeUnRecursion();		// 非递归方式
	void inOrderTree(Node *current);	// 递归方式

	// 先序遍历
	void preOrderTree();				// 递归方式
	void preOrderTreeUnRecursion();		// 非递归方式
	void preOrderTree(Node *current);	// 递归方式

	// 后序遍历
	void postOrderTree();				// 递归方式
	void postOrderTreeUnRecursion();	// 非递归方式
	void postOrderTree(Node *current);	// 递归方式

	// 按层次遍历二叉树
	void levelOrderTree();

	// 是否为二叉排序树
	bool isSortedTree(Tree tree);

	// 翻转二叉树
	void invertTree();

private:
	void insertNode(Node *current, int data);	// 递归插入方法
	Node *searchNode(Node *current, int data);	// 递归查找方法
	void deleteNode(Node *current);				// 递归删除方法
private:
	Node *root;	// 二叉排序树的根节点
};




void testInOrder();
void testPreOrder();
void testPostOrder();
void testLevelOrder();

#endif