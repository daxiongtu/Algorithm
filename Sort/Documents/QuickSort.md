## 快速排序

快速排序的基本思想是：通过一趟排序将要排序的数据分割成独立的两部分，其中一部分的所有数据都比另外一部分的所有数据都要小，然后再按此方法对这两部分数据分别进行快速排序，整个排序过程可以递归进行，以此达到整个数据变成有序序列。

快速排序是一种不稳定的排序算法，也就是说，多个相同的值的相对位置也许会在算法结束时产生变动

快速排序是C.R.A.Hoare于1962年提出的一种划分交换排序。它采用了一种分治的策略，通常称其为分治法(Divide-and-ConquerMethod)。

该方法的基本思想是：

1．先从数列中取出一个数作为基准数。

2．分区过程，将比这个数大的数全放到它的右边，小于或等于它的数全放到它的左边。

3．再对左右区间重复第二步，直到各区间只有一个数。

  

以一个数组作为示例，取区间第一个数为基准数。

0   1   2   3   4   5   6   7   8   9
72  6  57  88  60  42  83  73  48  85
初始时，i = 0;  j = 9;   X = a[i] = 72

由于已经将a[0]中的数保存到X中，可以理解成在数组a[0]上挖了个坑，可以将其它数据填充到这来。

从j开始向前找一个比X小或等于X的数。当j=8，符合条件，将a[8]挖出再填到上一个坑a[0]中。a[0]=a[8];i++; 这样一个坑a[0]就被搞定了，但又形成了一个新坑a[8]，这怎么办了？简单，再找数字来填a[8]这个坑。这次从i开始向后找一个大于X的数，当i=3，符合条件，将a[3]挖出再填到上一个坑中a[8]=a[3];j--;

 

数组变为：

0   1   2   3   4   5   6   7   8   9
48  6   57  88  60  42  83  73  88  85
 i = 3;   j = 7;   X = 72

再重复上面的步骤，先从后向前找，再从前向后找。

从j开始向前找，当j=5，符合条件，将a[5]挖出填到上一个坑中，a[3] = a[5]; i++;

从i开始向后找，当i=5时，由于i==j退出。

此时，i = j = 5，而a[5]刚好又是上次挖的坑，因此将X填入a[5]。

数组变为：

0   1   2   3   4   5   6   7   8   9
48  6   57  42  60  72  83  73  88  85
可以看出a[5]前面的数字都小于它，a[5]后面的数字都大于它。因此再对a[0…4]和a[6…9]这二个子区间重复上述步骤就可以了。


对挖坑填数进行总结

1．i =L; j = R; 将基准数挖出形成第一个坑a[i]。

2．j--由后向前找比它小的数，找到后挖出此数填前一个坑a[i]中。

3．i++由前向后找比它大的数，找到后也挖出此数填到前一个坑a[j]中。

4．再重复执行2，3二步，直到i==j，将基准数填入a[i]中。

```
#include<iostream>
using namespace std;

void QuickSort(int array[], int left, int right);
int main(int argc, const char **argv)
{
    int array[] = {34, 65, 12, 43, 67, 5, 78, 10, 3, 70}, k;
    int len = sizeof(array)/sizeof(int);
    cout << "The orginal arrayare:" << endl;
    for(k = 0; k < len; k++)
        cout << array[k] << ",";
    cout << endl;
    QuickSort(array, 0, len-1);

    cout << "The sorted arrayare:" << endl;
    for(k = 0; k < len; k++)
        cout << array[k] << ",";
    cout << endl;
    return 0;
}
 
void QuickSort(int array[], int left, int right)
{
    if (left < right)
    {      
        int i = left, j = right, x = array[left];
        while (i < j)
        {
            // 从右向左找第一个小于x的数
            while(i < j && array[j]>= x)  j--; 

            if(i < j) array[i++] = array[j];

            // 从左向右找第一个大于等于x的数
            while(i < j && array[i]< x)  i++; 

            if(i < j) array[j--] = array[i];
        }
        array[i] = x;

        // 递归调用
        QuickSort(array, left, i - 1); 
        QuickSort(array, i + 1, right);
    }

}
```