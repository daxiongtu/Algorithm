#include <stdio.h>

int numDupDigitsAtMostN(int N) {
	int index = 0;
	for(int i = 1; i < N; i++) {
		int array[10] = {0};
		int t = 0;
		int number = i;
		int isDump = 0;
		while(number) {
			t = number % 10;
			if (array[t] == 1) {
				isDump = 1;
				break;
			}

			array[t] = 1;
			number /= 10;
		}
		if (isDump) {
			array[index] = i;
			printf("%d, ", i);
			index++;
		}
	}
	return 0;

}

int dumpDigitst(int N) {
	for (int i = 1; i <= N; i++) {
		int bites = 0;
		int number = i;
		int offset = 0;
		int isDump = 0;
		while(number) {
			offset = number % 10;
			if ((bites >> offset & 1) == 1) {
				isDump = 1;
				break;
			}
			bites = (bites >> offset | 1) << offset;
			number /= 10;
		}
		if (isDump == 1) {
			printf("%d, ", i);
		}
	}
	return 0;
}
void printBinary(int number) {
	int delta = sizeof(int);
	for(int i = delta - 1; i >= 0; i--){
		int result = (number >> i) & 1;
		printf("%i", result);
	}
	printf("\n");

	
}
int main(int argc, const char **argv){
	
	dumpDigitst(10000000);

	printf("\n");

	return 0;
}