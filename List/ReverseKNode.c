/*
题目

这其实是一道变形的链表反转题，大致描述如下

给定一个单链表的头节点 head,实现一个调整单链表的函数，使得每K个节点之间为一组进行逆序，并且从链表的尾部开始组起，头部剩余节点数量不够一组的不需要逆序。（不能使用队列或者栈作为辅助）

例如： 链表:1->2->3->4->5->6->7->8->null, K = 3。那么 6->7->8，3->4->5，1->2各位一组。调整后：1->2->5->4->3->8->7->6->null。其中 1，2不调整，因为不够一组。

解答

这道题的难点在于，是从链表的尾部开始组起的，而不是从链表的头部，如果是头部的话，那我们还是比较容易做的，因为你可以遍历链表，每遍历 k 个就拆分为一组来逆序。但是从尾部的话就不一样了，因为是单链表，不能往后遍历组起。不过这道题肯定是用递归比较好做，对递归不大懂的建议看我之前写的一篇文章为什么你学不会递归？告别递归，谈谈我的一些经验，这篇文章写了关于递归的一些套路。


*/

#include <stdio.h>
#include <stdlib.h>

typedef struct ListNode{
	int data;
	struct ListNode *next;
}ListNode;

void printList(ListNode *head);

// 逆序链表非递归版本
ListNode *reverse_list(ListNode *head) {
	ListNode *next = NULL;	// 当前节点的后驱
	ListNode *pre = NULL;	// 当前节点的前驱
	while(head != NULL) {
		// 保存当前节点的后驱节点
		next = head->next;

		// 当前节点的后驱指向前驱
		head->next = pre;
		pre = head;

		// 处理下一个节点
		head = next;
	}
	return pre;
}

// 逆序链表
ListNode *reverseList(ListNode *head) {
	if (head == NULL || head->next == NULL) {
		return head;
	}

	ListNode *result = reverseList(head->next);
	head->next->next = head;
	head->next = NULL;
	return result;
}

// k 个为一组逆序
ListNode *reverseKGroup(ListNode  *head, int k) {
	ListNode *temp = head;
	for (int i = 1; i < k && temp != NULL; i++) {
		temp = temp->next;
	}

	// 判断节点的数量是否能够凑够一组
	if (temp == NULL) {
		return head;
	}

	ListNode *t2 = temp->next;
	temp->next = NULL;

	// 把当前的组进行逆序
	ListNode *newHead = reverseList(head);

	// 把之后的节点进行分组逆序
	ListNode *newTemp = reverseKGroup(t2, k);

	// 把两部分连接起来
	head->next = newTemp;

	return newHead;
}

ListNode *solve(ListNode *head, int k) {
	// 逆序链表
	head = reverseList(head);

	// 调用每K个为一组的逆序函数（从头部开始组起）
	head = reverseKGroup(head, k);

	// 再逆序一次
	head = reverseList(head);

	return head;
}

// 打印链表
void printList(ListNode *head) {
	while (head != NULL) {
		printf("%d\t", head->data);
		head = head->next;
	}
	printf("\n");
}

// 创建链表
void createList(ListNode *head, int array[], int count) {
	ListNode *p = head;

	for(int i = 0; i < count; i++) {
		ListNode *node = (ListNode *)malloc(sizeof(ListNode));
		if (node == NULL) {
			printf("申请链表结点内存失败\n");
			return;
		}
		node->data = array[i];
		node->next = NULL;
		p->next = node;	// 上一个节点指向这个新建立的节点
		p = node;
	}
}

void run() {
	int array[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	int count = sizeof(array)/sizeof(array[0]);

	// 创建头结点
	ListNode *head = (ListNode *)malloc(sizeof(ListNode));
	createList(head, array, count);
	// 删除临时创建的头结点
	ListNode *node = head->next;
	free(head);
	head = node;

	printList(head);

	solve(head, 3);

	printList(head);
}

int main(int argc, const char **argv) {
	run();
	return 0;
}