//
//  TwoSum.cpp
//  LeetcodeSet
//
//  Created by xujin on 2020/8/29.
//  Copyright © 2020 xujin. All rights reserved.
//

#include "TwoSum.hpp"

std::vector<int> TwoSumSolution::twoSum(std::vector<int> &nums, int target) {
    std::unordered_map <int,int> map;
    size_t size = nums.size();
    for(int i = 0; i < size; i++) {
        auto iter = map.find(target - nums[i]);
        if(iter != map.end()) {
            return {iter->second, i};
        }
        map.emplace(nums[i], i);
    }
    return {};
}

void TwoSumSolution::run() {
    std::cout << "Hello, World!\n";
    std::vector<int> source = {2, 7, 11, 15};
    int target = 13;
    std::vector<int> result = twoSum(source, target);
    std::cout << result[0] << " " << result[1]<< std::endl;
}
