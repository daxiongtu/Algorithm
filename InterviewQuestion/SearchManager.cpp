#include "SearchManager.h" 
#include <iostream>

using namespace std;

// 二维有序数组，查找
bool findWithBinary(int *array, int rows, int columns, int data) {
	if (array == NULL || rows < 1 || columns < 1) 
		return false;

	// 从二维数组右上角开始遍历
	int i = 0;
	int j = columns - 1;
	while(i < rows && j >= 0) {
		if (array[i * columns + j] == data) {
			return true;
		} else if(array[i * columns + j] > data) {
			// 当前遍历到的数组中的值大于data, 则data肯定不在这一行
			--j;
		} else {
			// 当前遍历到的数组中的值小于data, 则data肯定不在这一行
			++i;
		}
	}
	return false;
}

void testFindArrayElement() {
	int array[5][5] = {
		{0, 1, 2, 3, 4},
		{10, 11, 12, 13, 14},
		{20, 21, 22, 23, 24},
		{30, 31, 32, 33, 34},
		{40, 41, 42, 43, 44}
	};
	for (int i = 0; i < 45; i++) {
		cout << "find " << i << " ==> " << findWithBinary((int *)array, 5, 5, i) << endl;
	}
}