#include "StackManager.h"

#include <iostream>
#include <stack>
#include <limits>

using namespace std;

/*
 * 利用栈结构，实现获取一个最小元素，且push和pop 时间复杂度为O(1)
*/
class MyStack{
private:
	// 用于存存储栈中的元素
	stack<int> elemStack;

	// 栈顶永远存储当前elemStack最小值的元素
	stack<int> minStack;

public:
	void push(int data) {
		elemStack.push(data);

		// 更新保存最小元素的栈
		if (minStack.empty()) {
			minStack.push(data);
		} else {
			if (data < minStack.top()) {
				minStack.push(data);
			}
		}
	}

	int pop() {
		int topData = elemStack.top();
		elemStack.pop();

		if (topData == minData()) {
			minStack.pop();
		}
		return topData;
	}

	int minData() {
		if (minStack.empty()) {
			return numeric_limits<int>::max();
		} else {
			return minStack.top();
		}
	}
};

void testMyStack() {
	MyStack stack;
	stack.push(5);
	cout << "栈中最小值为: " << stack.minData() << endl;

	stack.push(6);
	cout << "栈中最小值为: " << stack.minData() << endl;

	stack.push(2);
	cout << "栈中最小值为: " << stack.minData() << endl;

	stack.push(1);
	cout << "栈中最小值为: " << stack.minData() << endl;

	stack.pop();
	cout << "栈中最小值为: " << stack.minData() << endl;
}

