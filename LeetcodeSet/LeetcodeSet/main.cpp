//
//  main.cpp
//  LeetcodeSet
//
//  Created by xujin on 2020/8/29.
//  Copyright © 2020 xujin. All rights reserved.
//

#include <iostream>
#include "TwoSum.hpp"
#include "AddTwoNumbers.hpp"
#include "LengthOfLongestSubstring.hpp"

int main(int argc, const char * argv[]) {
    // insert code here...
    
    TwoSumSolution *solution = new TwoSumSolution();
    solution->run();
    
    // 两数相加
    AddTwoNumbers *addTwoNum = new AddTwoNumbers();
    addTwoNum->run();
    
    // 计算最长不重复字符的子串的长度
    LengthOfLongestSubstring *substringLength = new LengthOfLongestSubstring();
    substringLength->run();
    return 0;
}
