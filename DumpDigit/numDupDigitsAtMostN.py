def numDupDigitsAtMostN2(N):
    T = [9,261,4725,67509,831429,9287109,97654149,994388229]
    t = [99,999,9999,99999,999999,9999999,99999999,999999999]
    if N < 10:
        return 0
    L = len(str(N))
    m, n = [1], []
    g = 11 - L
    for i in range(L):
        n.append(int(str(N)[i]))
        m.append(g)
        g = g * (12 - L + i)
    S = 0
    for i in range(L):
        if len(set(n[:L-i-1])) != len(n) - i - 1:
            continue
        k = 0
        for j in range(10):
            if j not in n[:L-i-1] and j > n[L-i-1]:
                k += 1
        S += k * m[i]

    return (T[L-2] - (t[L-2] - N - S))


# number = numDupDigitsAtMostN2(pow(10, 9) - 1)
# print(number)

def corpFlightBookings(self, bookings, n):
    """
    :type bookings: List[List[int]]
    :type n: int
    :rtype: List[int]
    """
    f = [0 for _ in range(n + 1)]
    for i, j, k in bookings:
        f[i-1] += k
        f[j] -= k

    for i in range(1, n+1):
        f[i] += f[i-1]
    return f[:-1]

def defangIPaddr(address):
    # return address.replace('.', '[.]')
    result = ""
    for char in address:
        if char == '.':
            result += '[.]'
        else:
            result += char
    return result

result = defangIPaddr('127.1.1.1')





