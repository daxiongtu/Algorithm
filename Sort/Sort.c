#include <stdio.h>
#include <string.h>
#include "Sort.h"


int ViolentMatch(const char* s, const char* p);
int KmpSearch(const char* s, const char* p);

void sort() {
	int array[8] = {3, 1, 7, 5, 2, 4, 9, 6};
	int count = sizeof(array)/sizeof(array[0]);
	QuickSort2(array, 0, count - 1);
	print(array, count, 0);

	const char *s = "BBC ABCDAB ABCDABCDABDE";
	const char *t = "ABCDABD";
	int index = ViolentMatch(s, t);
	printf("ViolentMatch result = %d\n", index);

	// kmp
	index = KmpSearch(s, t);
	printf("KmpSearch result = %d\n", index);
}


void print(int array[], int n, int i) {
	printf("%d\n", i);
	for (int j = 0; j < n; j++) {
		printf("%d ", array[j]);
	}
	printf("\n");
}

void swap(int *a,int *b){
    int t = *a;
    *a = *b;
    *b = t;
}

void InsertSort(int array[], int n) {
	for (int i = 1; i < n; i++) {
		//若第 i 个元素大于 i-1 元素则直接插入；反之，需要找到适当的插入位置后在插入。
		if (array[i] < array[i-1]) {
			int j = i - 1;
			int x = array[i];

			//采用顺序查找方式找到插入的位置，在查找的同时，将数组中的元素进行后移操作，给插入元素腾出空间
			while(j > - 1 && x < array[j]) {
				array[j+1] = array[j];
				j--;
			}

			// 插入正确的位置
			array[j+1] = x; 
		}
		print(array, n, i);
	}
}
void BubbleSort(int array[], int n) {
	for (int i = 0; i < n - 1; i++) {
		for (int j = i + 1; j < n - 1; j++) {
			if (array[j] < array[i]) {
				swap(&array[i], &array[j]);
			}
		}
	}
}

void BubbleSort2(int array[], int n) {
	for (int i = 0; i < n - 1; i++) {
		for (int j = n - 1; j >= i;j--) {

			//从后面到i个元素两两比较，把小的不断上顶 
			if (array[j - 1] > array[j]) {
				swap(&array[i], &array[j]);
			}
		}
	}
}
//挑选出最小值的下标
int selectMinKey(int array[],int n, int i) {
    int k = i;
    for(int j = i + 1;j < n; j++) {
        if(array[k] > array[j]) k = j;
    }
    return k;
}
//第i次循环找出i以后的数中的最小值，与当前第i项进行交换，每次循环的前i项是已经排好序的
void SelectSort(int array[], int n) {
    int key,temp;
    for(int i = 0 ;i < n; i++) {
        key = selectMinKey(array, n, i);
        if(key != i) {
            temp = array[i];
            array[i] = array[key];
            array[key] = temp;//将第i个位置的元素与最小值互换
        }
        print(array, n, i);
    }
}
// 快速排序
void QuickSort(int array[], int left, int right) {
	if (left < right) {      
		int i = left, j = right, x = array[left];
		while (i < j) {
			// 从右向左找第一个小于x的数
			while(i < j && array[j]>= x)  j--; 

			if(i < j) array[i++] = array[j];

			// 从左向右找第一个大于等于x的数
			while(i < j && array[i]< x)  i++; 

			if(i < j) array[j--] = array[i];
		}
		array[i] = x;

		// 递归调用
		QuickSort(array, left, i - 1); 
		QuickSort(array, i + 1, right);
	}

}


//快速排序一次划分算法
int Partition(int array[], int first, int end){

    int i,j;
    int temp;
    i = first;
    j = end;

    while (i < j){
        //进行右侧扫描
        while (i < j && array[i] < array[j])j--;
        if (i < j){
            swap(&array[i], &array[j]);
            i++;
        }
        //进行左侧扫描
        while (i < j && array[i] <= array[j]) i++;
        if (i < j){
            swap(&array[i], &array[j]);
            j--;
        }
    }

    return i;
}

//快速排序算法，递归调用
void QuickSort2(int array[], int first, int end){
    int pivot;
    if (first < end){
        pivot = Partition(array, first, end);

        QuickSort2(array, first, pivot - 1);
        QuickSort2(array, pivot + 1, end);
    }
}

int ViolentMatch(const char* s, const char* p)
{
	int sLen = strlen(s);
	int pLen = strlen(p);
 
	int i = 0;
	int j = 0;
	while (i < sLen && j < pLen) {
		if (s[i] == p[j]) {
			//①如果当前字符匹配成功（即S[i] == P[j]），则i++，j++    
			i++;
			j++;
		}
		else {
			//②如果失配（即S[i]! = P[j]），令i = i - (j - 1)，j = 0    
			i = i - j + 1;
			j = 0;
		}
	}
	//匹配成功，返回模式串p在文本串s中的位置，否则返回-1
	if (j == pLen)
		return i - j;
	else
		return -1;
}

//优化过后的next 数组求法
void GetNextval(const char* p, int next[])
{
	int pLen = strlen(p);
	next[0] = -1;
	int k = -1;
	int j = 0;
	while (j < pLen - 1) {
		//p[k]表示前缀，p[j]表示后缀  
		if (k == -1 || p[j] == p[k]) {
			++j;
			++k;
			//较之前next数组求法，改动在下面4行
			if (p[j] != p[k])
				next[j] = k;   //之前只有这一行
			else
				//因为不能出现p[j] = p[ next[j ]]，所以当出现时需要继续递归，k = next[k] = next[next[k]]
				next[j] = next[k];
		} else {
			k = next[k];
		}
	}
}
int KmpSearch(const char* s, const char* p)
{
	int i = 0;
	int j = 0;
	int sLen = strlen(s);
	int pLen = strlen(p);

	int next[pLen];
	GetNextval(p, next);
	while (i < sLen && j < pLen) {
		//①如果j = -1，或者当前字符匹配成功（即S[i] == P[j]），都令i++，j++    
		if (j == -1 || s[i] == p[j]) {
			i++;
			j++;
		} else {
			//②如果j != -1，且当前字符匹配失败（即S[i] != P[j]），则令 i 不变，j = next[j]    
			//next[j]即为j所对应的next值      
			j = next[j];
		}
	}
	if (j == pLen)
		return i - j;
	else
		return -1;
}


