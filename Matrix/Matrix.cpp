
#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>

#include "Matrix.h"

using namespace std;

// 输入一个矩阵，按照从外向里以顺时针的顺序依次打印出每一个数字
vector<int> printMatrix(vector<vector<int> > matrix);
void printMatrix() {
	std::vector<std::vector<int> > matrix;
	int dataList[4][4] = {
		{1 ,  2,  3, 4},
		{12, 13, 14, 5},
		{11, 16, 15, 6},
		{10,  9,  8, 7}
	};

	int n = 4;
	for (int i = 0; i < n; ++i) {
		std::vector<int> matrixItem;
		for (int j = 0; j < n; ++j) {
			matrixItem.push_back(dataList[i][j]);
		}
		matrix.push_back(matrixItem);
	}

	std::vector<int> result = printMatrix(matrix);

	
	for (vector<int>::const_iterator iter = result.cbegin(); iter != result.cend(); iter++) {
        cout << (*iter) << " ";
    }
}

/*
 生成一个螺旋矩阵
 通过观察发现矩阵的下标有这样一个规律：
 a行递增后b列递增然后c行递减再d列递减，但是对应值却是逐渐增加的。
 因此可用4个循环实现，需要注意的是在赋值时不要把之前的值覆盖了

 输出如下：
	1 ,  2,  3, 4
	12, 13, 14, 5
	11, 16, 15, 6
	10,  9,  8, 7
*/
void generateMatrix() {
	int matrix[4][4] = {0};
	int row = 0;
	int col = 0;

	int size = 4;
	// 起始值：起始值可以自定义
	int start = 4;
	int tmp = size;

	// size 阶的矩阵可以画出size/2个圈
	for (int count = 0; count < size/2; count++) {
		// a排赋值
		for(; col < tmp - 1; col++) { 
			matrix[row][col] = start++;
		}

		// b 排赋值
		for(; row < tmp - 1; row++) {
			matrix[row][col] = start++;
		}

		// c 排赋值
		for (col = tmp - 1; col > count; col--) {
			matrix[row][col] = start++;
		}

		// d 排赋值
		for (row = tmp - 1; row > count; row--) {
			matrix[row][col] = start++;
		}

		// 进入下一圈
		tmp--;
		row++;

		// 这是因为在换下一圈的时候回多加1
		start--;
	}
	// 如果size为奇数，则中间那一数遍历不到，这里补上
	if (size % 2 != 0) {
		matrix[row][col+1] = start + 1;
	}

	// 输出数组
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			cout << setw(3) << matrix[i][j];
		}
		cout << endl;
	}
}

void run() {
	generateMatrix();

}

vector<int> printMatrix(vector<vector<int> > matrix) {
	// 存储结果
	std::vector<int> result;

	// 边界条件
	if (matrix.empty()) {
		return result;
	}

	// 二位矩阵行列
	int rows = matrix.size();
	int cols = matrix[0].size();

	// 圈的四个角标
	int left = 0;
	int right = cols - 1;
	int top = 0;
	int bottom = rows - 1;

	// 循环打印圈
	while(left <= right && top <= bottom) {					// 循环条件：
		// 圈的第一步
		cout << "1: ";
		for(int i = left; i <= right; ++i) {				// 第一步循环条件
			result.push_back(matrix[top][i]);				// 第一步坐标
			cout << matrix[top][i] << " " ;
		}
		cout << endl;

		// 圈的第二步
		if (top < bottom) {									// 第二步边界条件
			cout << "2: ";
			for(int i = top + 1; i <= bottom; ++i) {		// 第二步循环条件
				result.push_back(matrix[i][right]);			// 第二步坐标
				cout << matrix[i][right] << " " ;
			}
			cout << endl;
		}

		// 圈的第三步
		if (top < bottom && left < right) {					// 第三步边界条件
			cout << "3: " ;
			for (int i = right - 1; i >= left; --i) {		// 第三步循环条件
				result.push_back(matrix[bottom][i]);		// 第三步坐标
				cout << matrix[bottom][i] << " " ;
			}
			cout << endl;
		}

		// 圈的第四步
		if (top + 1 < bottom && left < right) {				// 第四步边界条件
			cout << "4: ";
			for (int i = bottom - 1; i >= top + 1; --i) {	// 第四步循环条件
				result.push_back(matrix[i][left]);			// 第四步坐标
				cout << matrix[i][left] << " ";
			}
			cout << endl;
		}


		++left;
		--right;
		++top;
		--bottom;
	}

	return result;

}